# xslt preparation xml

Des feuilles de transformation xslt pour préparer un document xml à l'import dans indesign.

## Pagination (folio)
Si présente sous forme d'arguments @folio, la pagination est transformé en balise "page" et le numéro de page apparait dans le contenu sous la forme "page #". 

La pagination d'origine n'est pas isolée dans un paragraphe à part et peut être incluse dans un paragraphe standard, un titre ou tout type de bloc.

## Notes de bas de page ou fin de chapitre
Les attributs des appels et références de notes sont inculs dans le texte. Il sera nécessaire de les identifier et de recréer le lien.

Des formes courants de ces attributs sont apnb et nbbp.


## Usage

Lors de l'import d'un fichier .xml dans indesign, choisir l'option **Appliquer XSLT** et choisir le fichier avec le menu déroulant.

## Licence

GPL
