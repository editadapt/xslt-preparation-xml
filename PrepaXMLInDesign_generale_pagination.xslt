<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output method="xml" indent="no" xml:space="default"/>
    <xsl:strip-space elements="*"/>
    <xsl:template match="*">
        <xsl:element name="{local-name()}">
            <xsl:for-each select="@*">
                <xsl:attribute name="{local-name()}">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="p | tit | titre | titl | title | ftit | fstit | info | accro | collec | auteur | tit | stit | tome | partie | chapitre | contenu | section | théâtre | poésie">
        <xsl:element name="{local-name()}">
            <xsl:for-each select="@*">
                <xsl:attribute name="{local-name()}">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates/>
        </xsl:element>
        <xsl:text>
</xsl:text>
    </xsl:template>

    <xsl:template match="rp">
        <xsl:element name="{local-name()}">
            <xsl:for-each select="@folio">
             <pages>Page <xsl:value-of select="."/></pages>
            </xsl:for-each>
            <xsl:apply-templates/>
        </xsl:element>
        <xsl:text>
</xsl:text>
    </xsl:template>

    <xsl:template match="apnb">
        <xsl:element name="{local-name()}">
            <xsl:for-each select="@*">
             <appelnote><sup><xsl:value-of select="."/></sup></appelnote>
            </xsl:for-each>
            <xsl:apply-templates/>
        </xsl:element>
        <xsl:text>
</xsl:text>
    </xsl:template>

    <xsl:template match="ntb">
        <xsl:element name="{local-name()}">
            <xsl:for-each select="@*">
             <note>Note : <xsl:value-of select="."/></note>
            </xsl:for-each>
            <xsl:apply-templates/>
        </xsl:element>
        <xsl:text>
</xsl:text>
    </xsl:template>

</xsl:stylesheet>
